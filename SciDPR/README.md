# SciDPR for Precise Evidence Retrieval

## How to Use

```bash
# get into the folder
cd scidpr

# run the training script
./scripts/train.sh qasper scidpr 0,1,2,3,4,5,6,7
```
